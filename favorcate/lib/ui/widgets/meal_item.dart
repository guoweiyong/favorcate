import 'package:favorcate/core/model/meal_model.dart';
import 'package:favorcate/core/viewModel/favorite_view_model.dart';
import 'package:favorcate/ui/widgets/operation_item.dart';
import 'package:flutter/material.dart';
import 'package:favorcate/core/extension/int_extension.dart';
import 'package:provider/provider.dart';
class GYMealItem extends StatelessWidget {
  final GYMealModel _mealModel;
  const GYMealItem(this._mealModel,{Key? key}) : super(key: key);

  /**
   * 结构分析：
   * Card
   *  Colum
   *    Stack
   *      image
   *      Text
   *    Row
   *      icon Widget(自定义)
   *
   */

  @override
  Widget build(BuildContext context) {
    return Card(
      //设置卡片的四周边距
      margin: EdgeInsets.fromLTRB(10.px, 10.px, 10.px, 0),
      child: Column(
        children: [
          buildBasicInfo(context),
          buildOperationInfo()
        ],
      ),
    );
  }
  /*返回图片部分*/
  Widget buildBasicInfo(BuildContext context) {
      return ClipRRect(
        //裁剪图片的上面 左右两个圆角
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(8.px),
          topRight: Radius.circular(8.px)
        ),
        child: Stack(
            children: [
              Image.network(_mealModel.imageUrl ?? "", width: double.infinity,height: 250.px,fit: BoxFit.cover,),
              Positioned(
                right: 10,
                bottom: 10,
                child: Container(
                  child: Text(_mealModel.title ?? "", style: Theme.of(context).textTheme.headline2?.copyWith(
                    fontWeight: FontWeight.bold,
                    color: Colors.white
                  ),),
                  width: 250.px,
                  decoration: BoxDecoration(
                    color: Colors.black54,
                    borderRadius: BorderRadius.circular(8.px)
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                ),
              )
            ],
        ),
      );
  }
  /*返回下面操作部分*/
  Widget buildOperationInfo() {
    return Padding(
      padding: EdgeInsets.all(16.px),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          GYOperationItem(Icon(Icons.schedule), "${_mealModel.duration}分钟"),
          GYOperationItem(Icon(Icons.restaurant), "${_mealModel.complexityDes}"),
          Consumer<GYFavoriteViewModel>(
            builder: (ctx, viewModel, child) {
              //这里我们需要处理数据模型
              final iconData = viewModel.isFavorite(_mealModel) ? Icons.favorite : Icons.favorite_border;
              final iconColor = viewModel.isFavorite(_mealModel) ? Colors.red : Colors.black;
              final iconTitle = viewModel.isFavorite(_mealModel) ? "已收藏" : "未收藏";
                return GestureDetector(
                  child: GYOperationItem(Icon(iconData, color: iconColor,), iconTitle),
                  onTap: () {
                    viewModel.handleMealState(_mealModel);
                  },
                );
            },
          ),
        ],
      ),
    );
  }
}
