import 'package:flutter/material.dart';

class GYOperationItem extends StatelessWidget {
  /*左侧图片Widget*/
  Widget _icon;
  /*右侧标题*/
  String title;
  
  GYOperationItem(this._icon, this.title);
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Row(
      children: [
        _icon,
        SizedBox(width: 5,),
        Text(title)
      ],
    );
  }
}
