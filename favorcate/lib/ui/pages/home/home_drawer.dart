import 'package:favorcate/ui/pages/filter/filter_screen.dart';
import 'package:flutter/material.dart';
import 'package:favorcate/core/extension/int_extension.dart';

class GYHomeDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Drawer(
        child: Column(
          children: [
            buildTitleView(context),
            buildContent(context, Icon(Icons.restaurant),"进餐", (){
              //这里需要返回抽屉效，如何返回了？
              //其实抽屉效果底层也是使用Navigator实现的，所以我们只需要pop掉栈顶的元素就可以返回了
              Navigator.of(context).pop();
            }),
            buildContent(context, Icon(Icons.settings), "过滤", (){
              //跳转过滤界面
              Navigator.of(context).pushNamed(GYFilterScreen.routeName);
            })
          ],
        ),
      ),
      width: 250.px,
    );
  }

  Widget buildTitleView(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 120.px,
      color: Colors.orange,
      child: Text("开始动手", style: Theme.of(context).textTheme.headline2?.copyWith(
        fontWeight: FontWeight.bold
      ),),
      alignment: Alignment(0, 0.5),
      margin: EdgeInsets.only(bottom: 20.px),
    );
  }

  Widget buildContent(BuildContext context, Widget icon, String content, GestureTapCallback handle) {
    return ListTile(
      leading: icon,
      title: Text(content, style: Theme.of(context).textTheme.headline3,),
      onTap: handle,
    );
  }
}