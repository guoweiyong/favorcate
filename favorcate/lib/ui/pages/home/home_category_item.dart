
import 'package:favorcate/core/model/category_model.dart';
import 'package:favorcate/ui/pages/meal/meal_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GYHomeCategoryItem extends StatelessWidget {
  final GYCategoryModel _item;

  GYHomeCategoryItem(this._item);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(12),
            //设置item的颜色为渐变色
            gradient: LinearGradient(
                colors: [
                  _item.backgroundColor.withOpacity(.5),
                  _item.backgroundColor
                ]
            )
        ),
        alignment: Alignment.center,
        child: Text(_item.title ?? "", style: Theme
            .of(context)
            .textTheme
            .headline3
            ?.copyWith(
            fontWeight: FontWeight.bold
        ),),
      ),
      onTap: () {
        //这里需要跳转到具体的meal页面, 并且传递一个参数过去
        Navigator.of(context).pushNamed(GYMealScreen.routeName, arguments: _item);
      },
    );
  }
}

