import 'package:favorcate/core/model/category_model.dart';
import 'package:favorcate/ui/pages/home/home_category_item.dart';
import 'package:flutter/material.dart';
import 'package:favorcate/core/extension/int_extension.dart';
import 'package:favorcate/core/service/json_parse.dart';

class GYHomeContent extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<GYCategoryModel>>(
        future: GYJsonParse.getCategryData(),
        /**
         * 在某些情况下FutureBuilder这个widget可以代替StatefulWidget来使用，使用代码看起来更加简洁
         * 但是该widget有局限性：
         * 1、如果该页面需要经常刷新数据，可能需要缓存数据， 那么该widget就无法满足， 如果该页面多次刷新，那么使用FutureBuilder会造成多次发送请求
         * 2、如果该页面需要实现 上拉加载功能，  该widget也无法完成
         */

        builder: (context, snapshotData) {
//hasData： 表示，如果请求加载的数据回来了，返回true ，否则返回false
          if (!snapshotData.hasData)
            return Center(
              child: CircularProgressIndicator(),
            );
//如果请求错误，则显示错误页面
          if (snapshotData.error != null) return Center(child: Text("请求失败"));
          final items = snapshotData.data;

          return GridView.builder(
              padding: EdgeInsets.all(20.px),
              itemCount: items!.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2, //交叉轴方向显示2个item
                  childAspectRatio: 1.5, //宽高比
                  crossAxisSpacing: 20.px, //交叉轴item之间的距离
                  mainAxisSpacing: 20.px //主轴方向item之间的距离
                  ),
              itemBuilder: (context, index) {
                return GYHomeCategoryItem(items[index]);
              });
        });
  }
}
