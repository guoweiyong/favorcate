import 'package:flutter/material.dart';

// flutter还可以这样继承自widget
class GYHomeAppBar extends AppBar {

  GYHomeAppBar(BuildContext context) : super(title: Text("美食广场"),
    centerTitle: true,
    leading: IconButton(
        icon: Icon(Icons.build),
        onPressed: (){
          //按钮点击方法
          Scaffold.of(context).openDrawer();
        }
    )
      //在home页面增加drawer页面，需要如下实现代码
    // Builder(
    //   builder:(ctx) {
    //     return IconButton(
    //         icon: Icon(Icons.build),
    //         onPressed: (){
    //           //按钮点击方法
    //           Scaffold.of(ctx).openDrawer();
    //         }
    //     );
    //   },
    // ),
  );
}