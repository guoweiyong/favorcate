import 'package:favorcate/ui/pages/home/home_appbar.dart';
import 'package:favorcate/ui/pages/home/home_content.dart';
import 'package:favorcate/ui/pages/home/home_drawer.dart';
import 'package:flutter/material.dart';



class GYHomeScreen extends StatelessWidget {
  const GYHomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GYHomeAppBar(context),
      //表哥布局
      body: GYHomeContent(),
      //drawer: GYHomeDrawer(), 这里弹出的drawer没有盖住底部tabbar工具栏， 因为我是在home页面弹出drawer，
      // home页面本身不包括底部tabbar，要想盖住底部工具栏，需要再main_screen页面增加抽屉效果
    );
  }
}
