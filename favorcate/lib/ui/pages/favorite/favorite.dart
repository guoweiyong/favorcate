import 'package:favorcate/core/viewModel/favorite_view_model.dart';
import 'package:favorcate/ui/widgets/meal_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:favorcate/ui/pages/detailes/meal_item_detail.dart';

class GYFavoriteScreen extends StatelessWidget {
  const GYFavoriteScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("我的收藏"),),
      body: GYFavoriteContent(),
    );
  }
}

class GYFavoriteContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Consumer<GYFavoriteViewModel>(
        builder: (ctx, viewModel, child) {
          print("viemodel========${viewModel.meals}");
          print("filterViewmodel========${viewModel.filterMeals}");
          return ListView.builder(
            itemCount: viewModel.filterMeals.length,
              itemBuilder: (ctx, index) {
                  return GestureDetector(
                      child: GYMealItem(viewModel.filterMeals[index]),
                      //点击跳转页面详情
                    onTap: () => Navigator.of(context).pushNamed(GYMealItemDetailScreen.routeName, arguments: viewModel.filterMeals[index])
                  );
              },
          );
        }
    );
  }
}