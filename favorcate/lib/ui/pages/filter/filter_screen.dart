import 'package:flutter/material.dart';
import 'filter_content.dart';
class GYFilterScreen extends StatelessWidget {
  static String routeName = "/filter";

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(title: Text("美食过滤"),),
      body: GYFilterContent(),
    );
  }
}