import 'package:favorcate/core/viewModel/filter_view_model.dart';
import 'package:flutter/material.dart';
import 'package:favorcate/core/extension/int_extension.dart';
import 'package:provider/provider.dart';
class GYFilterContent extends StatelessWidget {
  final List<String> _titles = ["无谷蛋白","不含乳糖","普通素食者","严格素食者"];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: [
        buildHeadTitle(context),
        buildChooseSelected()
      ],
    );
  }

  /*返回头部标签*/
  Widget buildHeadTitle(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 16.px),
      alignment: Alignment.center,
      child: Text("展示你的选择", style: Theme.of(context).textTheme.headline2?.copyWith(
        fontWeight: FontWeight.bold
      ),),
    );
  }

  Widget buildChooseSelected() {
    /**
     * Expanded: 如果内容太少，会把你的显示区域扩大， 如果你的显示内容超过显示区域， 那么Expanded会把内容的显示区域缩小至可以显示的区域大小
     */
    return Consumer<GYFilterViewModel>(
      builder: (ctx, filterViewModel,child) {
        return Expanded(
          child: ListView.builder(
              itemCount: _titles.length,
              itemBuilder: (ctx, index) {
                return ListTile(
                  title: Text(_titles[index]),
                  subtitle: Text("展示${_titles[index]}食物"),
                  trailing: Switch(
                    value: getChooseState(index, filterViewModel),
                    onChanged: (state) {
                      //switch状态改变方法
                      updateChooseState(index, filterViewModel, state);
                    },
                  ),
                );
              }
          ),
        );
      },
    );
  }

  //更新选择状态
  void updateChooseState(int index, GYFilterViewModel filterViewModel, bool state) {
    switch (index) {
      case 0:
        filterViewModel.isGlutenFree = state;
        break;
      case 1:
        filterViewModel.isLactoseFree = state;
        break;
      case 2:
        filterViewModel.isVegetarian = state;
        break;
      case 3:
        filterViewModel.isVegan = state;
        break;
      default:
        break;
    }
  }

  bool getChooseState(int index, GYFilterViewModel filterViewModel) {
    switch (index) {
      case 0:
        return filterViewModel.isGlutenFree;
        break;
      case 1:
        return filterViewModel.isLactoseFree;
        break;
      case 2:
        return filterViewModel.isVegetarian;
        break;
      case 3:
        return filterViewModel.isVegan;

      default:
        return false;
    }
  }
}