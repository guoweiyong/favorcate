import 'package:favorcate/core/model/category_model.dart';
import 'package:favorcate/ui/pages/meal/meal_content.dart';
import 'package:flutter/material.dart';

class GYMealScreen extends StatelessWidget {
  //对应的路由名称
  static String routeName = "/meal";

  const GYMealScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //获取跳转传递过来的参数,并通过as 转换乘具体的模型
    final _catetoryItem = ModalRoute.of(context)?.settings.arguments as GYCategoryModel;

    return Scaffold(
      appBar: AppBar(title: Text(_catetoryItem.title ?? "标题为空"), centerTitle: true,),
      body: GYMealContent(_catetoryItem),
    );
  }
}
