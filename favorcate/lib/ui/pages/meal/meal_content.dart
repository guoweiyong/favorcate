import 'package:favorcate/core/model/category_model.dart';
import 'package:favorcate/core/model/meal_model.dart';
import 'package:favorcate/core/viewModel/meal_viewmodel.dart';
import 'package:favorcate/ui/pages/detailes/meal_item_detail.dart';
import 'package:favorcate/ui/widgets/meal_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:collection/collection.dart';

class GYMealContent extends StatelessWidget {
  final GYCategoryModel _categoryModel;
  //初始化方法传递过来
  GYMealContent(this._categoryModel);

  // 使用Consumer的方式来实现  共享数据的展示和获取
  // @override
  // Widget build(BuildContext context) {
  //   return Consumer<GYMealViewModel>(
  //     /**
  //      * 第一个参数： context 上下文
  //      * 第二个参数： ChangeNotifier对应的实例，共享的数据，也是我们在build函数中使用的主要对象
  //      * 第三个参数： child：目的是进行优化把不希望重新build的子widget层放到child属性之后，这样更新数据之后就不会重新build整个widget树
  //      */
  //       builder: (context, mealViewModel, child) {
  //           //根据id筛选出数据
  //         final meals = mealViewModel.meals.where((element) => element.categories!.contains(_categoryModel.id)).toList();
  //         return ListView.builder(itemBuilder: (context, index,) {
  //           return Text(meals[index].title ?? "名称为空");
  //         }, itemCount: meals.length,);
  //       }
  //   );
  // }

  // 上面使用Consumer的方式来实现共享数据的获取， 这里其实使用Selector的方式来实现更加的简介方便
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Selector<GYMealViewModel, List<GYMealModel>>(
      builder: (context, meals, child) {
        return ListView.builder(
          itemBuilder: (
            context,
            index,
          ) {
            return GestureDetector(
              child: GYMealItem(meals[index]),
              onTap: (){
                //每个卡片都增加一个点击手势，点击跳转详情
                Navigator.of(context).pushNamed(GYMealItemDetailScreen.routeName, arguments: meals[index]);
              },
            );
          },
          itemCount: meals.length,
        );
      },
      //这里是把 A 转换成 S
      selector: (context, mealVM) {
        return mealVM.filterMeals
            .where((element) => element.categories!.contains(_categoryModel.id))
            .toList();
      },
      shouldRebuild: (pre, next) {
        //比较两个数组是否完全一样 ，一样则不重新build， 如果不一样则重新build
        return !ListEquality().equals(pre, next);
      },
    );
  }
}