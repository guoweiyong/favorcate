
import 'package:favorcate/ui/pages/favorite/favorite.dart';
import 'package:favorcate/ui/pages/home/home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/*页面pages数组*/
final List<Widget> pages = [
  GYHomeScreen(),
  GYFavoriteScreen()
];

/*底部工具栏Items 数组*/
final List<BottomNavigationBarItem> items = [
  BottomNavigationBarItem(icon: Icon(Icons.home), label: "首页"),
  BottomNavigationBarItem(icon: Icon(Icons.star), label: "收藏")
];