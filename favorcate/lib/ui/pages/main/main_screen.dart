
import 'package:favorcate/ui/pages/home/home_drawer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:favorcate/ui/pages/main/initialize_item.dart';

class GYMainScreen extends StatefulWidget {
  /*route名称*/
  static final String routeName = "/";

  const GYMainScreen({Key? key}) : super(key: key);

  @override
  _GYMainScreenState createState() => _GYMainScreenState();
}

class _GYMainScreenState extends State<GYMainScreen> {

  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: GYHomeDrawer(),
      body: IndexedStack(
        index: _currentIndex,
        children: pages,
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: items,
        currentIndex: _currentIndex,
        //取消点击选中字体变大变小的效果
        selectedFontSize: 14,
        unselectedFontSize: 14,
        onTap: (index){
          setState(() {
            _currentIndex = index;
          });
        },
      ),
    );
  }
}
