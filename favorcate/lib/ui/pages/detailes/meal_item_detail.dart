import 'package:favorcate/core/model/meal_model.dart';
import 'package:favorcate/core/viewModel/favorite_view_model.dart';
import 'package:favorcate/ui/pages/detailes/detail_content.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class GYMealItemDetailScreen extends StatelessWidget {
  /*跳转路由标记*/
  static String routeName = "/meal_item_detail";

  @override
  Widget build(BuildContext context) {
    final GYMealModel mealModel = ModalRoute.of(context)?.settings.arguments as GYMealModel;
    return Scaffold(
      appBar: AppBar(title: Text(mealModel.title ?? "未知名称"),),
      body: GYItemDetailContent(mealModel),
      floatingActionButton: Consumer<GYFavoriteViewModel>(
        builder: (ctx, viewModel, child) {
          //这里我们需要处理数据模型
          final iconData = viewModel.isFavorite(mealModel) ? Icons.favorite : Icons.favorite_border;
          final iconColor = viewModel.isFavorite(mealModel) ? Colors.red : Colors.black;

          return FloatingActionButton(
            child: Icon(iconData, color: iconColor,),
            onPressed: (){
              viewModel.handleMealState(mealModel);
            },
          );
        },
      ),
    );
  }
}
