import 'package:favorcate/core/model/meal_model.dart';
import 'package:favorcate/ui/share/size_fit.dart';
import 'package:flutter/material.dart';
import 'package:favorcate/core/extension/int_extension.dart';

class GYItemDetailContent extends StatelessWidget {
  final GYMealModel _mealModel;

  GYItemDetailContent(this._mealModel);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SingleChildScrollView(
      child: Column(
        children: [
          buildHeadImage(),
          buildTitle(context, "制作材料"),
          buildMaterials(context),
          buildTitle(context, "步骤"),
          buildSetup(context),
          SizedBox(height: GYSizeFit.instance.bottomHeight,)
        ],
      ),
    );
  }

  /*返回头部视图*/
  Widget buildHeadImage() {
    return Container(
      width: double.infinity,
        child: Image.network(_mealModel.imageUrl ?? "", height: 250.px, fit: BoxFit.cover,),
    );
  }

  /*制作材料widget*/
  Widget buildMaterials(BuildContext context) {
    return buildMakeContent(context,
      ListView.builder(
          shrinkWrap: true,//  默认值：false ， false：尽可能占据多的高度， true: 包裹内容
          physics: NeverScrollableScrollPhysics(),//控制列表是否可能滚动的属性，NeverScrollableScrollPhysics： 设置列表不滚动
          padding: EdgeInsets.zero, //设置shrinkWrap属性后， 列表下方会多出一段边距， 我们直接手动设置边距的值，就可以解决这个问题
          itemCount: _mealModel.ingredients?.length,
          itemBuilder: (cxt, index) {
            return Card(
              color: Theme.of(cxt).accentColor,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                child: Text(_mealModel.ingredients?[index] ?? "数据出错"),
              ),

            );
          }
      ),
    );
  }

  /*制作步骤*/
  Widget buildSetup(BuildContext context) {
    return buildMakeContent(context,
        ListView.builder(
        shrinkWrap: true,//  默认值：false ， false：尽可能占据多的高度， true: 包裹内容
        physics: NeverScrollableScrollPhysics(),//控制列表是否可能滚动的属性，NeverScrollableScrollPhysics： 设置列表不滚动
        padding: EdgeInsets.zero, //设置shrinkWrap属性后， 列表下方会多出一段边距， 我们直接手动设置边距的值，就可以解决这个问题
        itemCount: _mealModel.steps?.length,
        itemBuilder: (cxt, index) {
          return ListTile(
            leading: CircleAvatar(child: Text("#${index+1}")),
            title: Text(_mealModel.steps?[index] ?? "数据出错"),
          );
        }
    ));
  }

/**
 * 公共方法
 */

  /*得到一个标题*/
  Widget buildTitle(BuildContext context, String title) {
    return Padding(
      padding: EdgeInsets.all(16.px),
      child: Text(title,
        style: Theme.of(context).textTheme.headline2?.copyWith(
        fontWeight: FontWeight.bold,
        color: Colors.black
      ),
      ),
    );
  }

  Widget buildMakeContent(BuildContext context, Widget child) {
    return Container(
      width: MediaQuery.of(context).size.width - 30.px, //设置宽度
      padding: EdgeInsets.all(8.px),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(8.px),
      ),
      child: child
    );
  }
}