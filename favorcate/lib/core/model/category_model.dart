import 'package:flutter/material.dart';

class GYCategoryModel {
  String? id; //存在一个默认值null ，所以需要声明为？ 可选类型
  String? title;
  String? color;
  /*该item的背景颜色， 默认是orange*/
  Color backgroundColor = Colors.orange;

  GYCategoryModel({this.id, this.title, this.color});

  GYCategoryModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    color = json['color'];

    //1.将得到的颜色字符串，转换成16进制的数字
    if (color != null) {
      /**
       * int.Parse()抛出了异常，原因是int.Parse()是一种类容转换；表示将数字内容的字符串转为int类型。
       * 如果字符串为空，则抛出ArgumentNullException异常
       * 如果字符串内容不是数字，则抛出FormatException异常；
       * 如果字符串内容所表示数字超出int类型可表示的范围，则抛出OverflowException异常
       */
      /**
       *  int中有一个parse方法来解析字符串 会抛出FormatException异常 radix:默认基数10进制，我们需要指定是16进制
       */
      final colorInt = int.parse(color!, radix: 16);
      //把透明度直接通过 或  的方式加进去 ，然后调用Color的构造方法得到一个有透明度的颜色
      backgroundColor = Color(colorInt | 0xFF000000);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['color'] = this.color;
    return data;
  }
}

