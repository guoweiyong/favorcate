
import 'dart:convert';

GYMealModel gyMealModelFromJson(String str) => GYMealModel.fromJson(json.decode(str));

String gyMealModelToJson(GYMealModel data) => json.encode(data.toJson());

class GYMealModel {
  GYMealModel({
    this.id,
    this.categories,
    this.title,
    this.affordability,
    this.complexity,
    this.imageUrl,
    this.duration,
    this.ingredients,
    this.steps,
    this.isGlutenFree,
    this.isVegan,
    this.isVegetarian,
    this.isLactoseFree,
    this.complexityDes
  });

  String? id;
  List<String>? categories = [];
  String? title;
  int? affordability;
  /*难度系数*/
  int? complexity;
  String? imageUrl;
  int? duration;
  List<String>? ingredients;
  List<String>? steps;
  bool? isGlutenFree;
  bool? isVegan;
  bool? isVegetarian;
  bool? isLactoseFree;


  //自增属性
  String? complexityDes;

  factory GYMealModel.fromJson(Map<String, dynamic> json) => GYMealModel(
    id: json["id"],
    categories: List<String>.from(json["categories"].map((x) => x)),
    title: json["title"],
    affordability: json["affordability"],
    complexity: json["complexity"],
    complexityDes: (json["complexity"] == 0 ? "简单" : (json["complexity"] == 1 ? "中等" : "困难")),
    imageUrl: json["imageUrl"],
    duration: json["duration"],
    ingredients: List<String>.from(json["ingredients"].map((x) => x)),
    steps: List<String>.from(json["steps"].map((x) => x)),
    isGlutenFree: json["isGlutenFree"],
    isVegan: json["isVegan"],
    isVegetarian: json["isVegetarian"],
    isLactoseFree: json["isLactoseFree"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "categories": List<dynamic>.from((categories ?? []).map((x) => x)),
    "title": title,
    "affordability": affordability,
    "complexity": complexity,
    "imageUrl": imageUrl,
    "duration": duration,
    "ingredients": List<dynamic>.from((ingredients ?? []).map((x) => x)),
    "steps": List<dynamic>.from((steps ?? []).map((x) => x)),
    "isGlutenFree": isGlutenFree,
    "isVegan": isVegan,
    "isVegetarian": isVegetarian,
    "isLactoseFree": isLactoseFree,
  };
}
