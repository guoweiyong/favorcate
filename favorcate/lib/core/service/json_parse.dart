
import 'dart:convert';
import 'package:favorcate/core/model/category_model.dart';
import 'package:flutter/services.dart';
import 'package:favorcate/core/model/meal_model.dart';

class GYJsonParse {
  /*解析本地的json文件，并返回一个对应的 数据模型列表*/
  static Future<List<GYCategoryModel>> getCategryData() async {
    //1. 加载json文件，返回一个字符串
    String jsonString = await rootBundle.loadString("assets/json/category.json");

    //2. 把字符串转换成为json // 返回值dynamic对象
    final jsonResult = json.decode(jsonString);
    //3.获取list数据
    final resultList = jsonResult["category"];

    //声明一个空的list数组， 在flutter2.2之后 new List() 方式创建List方式已经被弃用
    List<GYCategoryModel> categoryItems = [];
    //4。将Map中的数据转换成具体的数据模型
    for (var json in resultList) {
        categoryItems.add(GYCategoryModel.fromJson(json));
    }
    return categoryItems;
  }

  /*加载本地的菜谱分类数据， 也是采用加载json文件方式， 实际上这里需要采用网络请求方式合适些*/
  static Future<List<GYMealModel>> getMealData() async {
    //1.加载json文件，返回一个字符串
    String jsonString = await rootBundle.loadString("assets/json/meal.json");

    //2.把字符串转换成json，该方法返回的是dynamic对象
    final jsonResult = json.decode(jsonString);
    //3.获取list文件
    final resultList = jsonResult["meal"];
    List<GYMealModel> meals = [];
    for (var json in resultList) {
      meals.add(GYMealModel.fromJson(json));
    }

    return meals;
  }
}