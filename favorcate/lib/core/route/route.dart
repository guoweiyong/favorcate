import 'package:favorcate/ui/pages/detailes/meal_item_detail.dart';
import 'package:favorcate/ui/pages/filter/filter_screen.dart';
import 'package:favorcate/ui/pages/main/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:favorcate/ui/pages/meal/meal_screen.dart';

class GYRoute {
  //初始路由指定界面
  static final String initialRoute = GYMainScreen.routeName;

  // 公共路由数组: 增加一个新的页面就需要配置这个数组
  static final Map<String, WidgetBuilder> routes = {
    GYMainScreen.routeName:(context) => GYMainScreen(),
    GYMealScreen.routeName:(context) => GYMealScreen(),
    GYMealItemDetailScreen.routeName: (context) => GYMealItemDetailScreen()
  };

  //自己扩展
  static final RouteFactory generateRoute = (settings) {
    if (settings.name == GYFilterScreen.routeName) {
      return MaterialPageRoute(
          builder: (context) {
            return GYFilterScreen();
          },
        fullscreenDialog: true,
      );
    }
    return null;
  };

  //位置页面、错误页面
  static final RouteFactory unknownRoute = (settings) {
    return null;
  };
}