import 'package:favorcate/ui/share/size_fit.dart';

extension intFit on int {
  double get px {
    return this.toDouble() * GYSizeFit.instance.px;
  }

  double get dpx {
    return this.toDouble() * GYSizeFit.instance.dpr;
  }
}