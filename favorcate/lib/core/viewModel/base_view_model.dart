import 'package:flutter/material.dart';
import 'package:favorcate/core/model/meal_model.dart';
import 'package:favorcate/core/viewModel/filter_view_model.dart';
class GYBaseViewModel extends ChangeNotifier {
  /*需要共享的数据*/
  List<GYMealModel> _meals = [];
  /*依赖的数据模型*/
  GYFilterViewModel? _filterViewModel;

  void updateFilterViewModel(GYFilterViewModel filterViewModel) {
    _filterViewModel = filterViewModel;
  }

  /*提供get方法*/
  List<GYMealModel> get meals {
    return _meals;
  }

  set meals(List<GYMealModel> value) {
    _meals = value;
    notifyListeners();
  }

  /*返回筛选的数据*/
  List<GYMealModel> get filterMeals {
    return _meals.where((meal) {
      if (_filterViewModel == null) return false;
      if (_filterViewModel!.isGlutenFree && !(meal.isGlutenFree ?? false)) return false;
      if (_filterViewModel!.isLactoseFree && !(meal.isLactoseFree ?? false)) return false;
      if (_filterViewModel!.isVegan && !(meal.isVegan ?? false)) return false;
      if (_filterViewModel!.isVegetarian && !(meal.isVegetarian ?? false)) return false;

      return true;
    }).toList();
  }
}