import 'package:flutter/material.dart';

class GYFilterViewModel extends ChangeNotifier {
  /*是否无谷蛋白*/
  bool _isGlutenFree = false;
  /*是否严格的素食主义者*/
  bool _isVegan = false;
  /*是否素食主义者*/
  bool _isVegetarian = false;
  /*是否乳糖*/
  bool _isLactoseFree = false;

  set isGlutenFree(bool value) {
    _isGlutenFree = value;
    notifyListeners();
  }

  set isVegan(bool value) {
    _isVegan = value;
    notifyListeners();
  }

  set isLactoseFree(bool value) {
    _isLactoseFree = value;
    notifyListeners();
  }

  set isVegetarian(bool value) {
    _isVegetarian = value;
    notifyListeners();
  }

  bool get isLactoseFree => _isLactoseFree;

  bool get isVegetarian => _isVegetarian;

  bool get isVegan => _isVegan;

  bool get isGlutenFree => _isGlutenFree;
}