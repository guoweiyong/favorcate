import 'package:favorcate/core/service/json_parse.dart';
import 'package:favorcate/core/viewModel/base_view_model.dart';


/**
 * 我们需要把各个菜普的具体信息，做成一个共享数据， 因为很多界面都需要这份数据， 所以这里使用Provider来实现一个数据的共享
 * */

class GYMealViewModel extends GYBaseViewModel {

  //在初始化方法中获取数据
  GYMealViewModel() {
    //获取菜谱详细数据，本来是网络请求获取数据，目前是获取本地json文件的数据
    GYJsonParse.getMealData().then((value) {
      meals = value;
    });
  }
}