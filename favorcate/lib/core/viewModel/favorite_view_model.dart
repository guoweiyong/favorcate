import 'package:favorcate/core/model/meal_model.dart';
import 'package:favorcate/core/viewModel/base_view_model.dart';

class GYFavoriteViewModel extends GYBaseViewModel {

  /*添加收藏*/
  void addFavorite(GYMealModel mealModel) {
    meals.add(mealModel);
    //需要通知
    notifyListeners();
  }

  /*删除收藏*/
  void removeFavorite(GYMealModel mealModel) {
    meals.remove(mealModel);
    //需要通知
    notifyListeners();
  }

  /*处理数据的收藏状态, 如果是收藏，则删除收藏，  否则添加收藏*/
  void handleMealState(GYMealModel mealModel) {
    if (this.isFavorite(mealModel)) {
      removeFavorite(mealModel);
    } else {
      addFavorite(mealModel);
    }
  }

  /*判断该Item是否被收藏*/
  bool isFavorite(GYMealModel mealModel) {
    return meals.contains(mealModel);
  }
}