import 'package:favorcate/core/route/route.dart';
import 'package:favorcate/core/viewModel/favorite_view_model.dart';
import 'package:favorcate/core/viewModel/filter_view_model.dart';
import 'package:favorcate/core/viewModel/meal_viewmodel.dart';
import 'package:flutter/material.dart';

import 'package:favorcate/ui/share/app_theme.dart';
import 'package:provider/provider.dart';

void main() {
  //// Provider -> ViewModel/Provider/Consumer(Selector)
  runApp(
    //使用ChangeNotifierProvider作为顶层，不管在那里都可以访问这个共享数据
    MultiProvider( //多个Provider
      providers: [
        ChangeNotifierProvider(create: (ctx) => GYFilterViewModel()),
        //这里不会再启动App的时候就加载数据的， 是在第一次使用到共享数据的时候才会加载，是一个懒加载
        //ChangeNotifierProvider(create: (ctx) => GYMealViewModel()),
        //目前为了实现过滤的效果，GYMealViewModel依赖于GYFilterViewModel的数据，所以这里我们不能在使用单独的Provider了，需要使用依赖的Provider
        ChangeNotifierProxyProvider<GYFilterViewModel, GYMealViewModel>(
            create: (ctx) => GYMealViewModel(),
            update: (ctx, filterViewModel, mealViewModel) {
              //需要设置对应的筛选对象
              mealViewModel!.updateFilterViewModel(filterViewModel);

              return mealViewModel;
            }
        ),
        //ChangeNotifierProvider(create: (ctx) => GYFavoriteViewModel()),
        ChangeNotifierProxyProvider<GYFilterViewModel, GYFavoriteViewModel>(
            create: (ctx) => GYFavoriteViewModel(),
            update: (ctx, filterVM, favoriteVM) {
              //设置筛选对象
              favoriteVM!.updateFilterViewModel(filterVM);

              return favoriteVM;
            }
        ),
      ],
      child: MyApp(),
    )
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '美食广场',
      // 设置主题
      theme: GYAppTheme.norTheme,

      //设置路由信息
      initialRoute: GYRoute.initialRoute,
      routes: GYRoute.routes,//路由数组
      onGenerateRoute: GYRoute.generateRoute,//路由的钩子函数， 当你跳转一个路由时，路由列表中找不到对应路由的映射关系时候，就回调用此函数
      onUnknownRoute: GYRoute.unknownRoute, //指定错误界面
    );
  }
}

